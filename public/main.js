const {
  ipcMain,
  app,
  BrowserWindow,
  Menu,
} = require('electron');
const isMac = process.platform === 'darwin';

const menuTemplate = [ ...(isMac ? [ {
  label: app.name,
  submenu: [
    { role: 'about' },
    { type: 'separator' },
    { role: 'services' },
    { type: 'separator' },
    { role: 'hide' },
    { role: 'hideothers' },
    { role: 'unhide' },
    { type: 'separator' },
    { role: 'quit' },
  ],
} ] : []) ];

// Right after the line where you changed the document.location
function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1000,
    height: 650,
    minWidth: 1000,
    minHeight: 650,
    webPreferences: {
      nodeIntegration: true,
    },
    title: 'Planning Poker',
    transparent: true,
    vibrancy: 'appearance-based',
    titleBarStyle: 'hiddenInset',
    frame: false,
  });

  win.setTrafficLightPosition({
    x: 18,
    y: 36,
  });

  //load the index.html from a url
  win.loadURL(process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'http://planning-poker-electron.s3-website-eu-west-1.amazonaws.com');

  // Open the DevTools.
  // win.webContents.openDevTools()

  ipcMain.on('updateMenu', (event, options) => {
    const template = options.map(menuItem => {
      if (menuItem && menuItem.submenu) {
        menuItem.submenu = menuItem.submenu.map(submenuItem => {
          if (submenuItem && submenuItem.click && typeof submenuItem.click === 'object') {
            const click = { ...submenuItem.click };
            submenuItem.click = () => {
              if (!win) {
                createWindow();
                return;
              }

              if (click.type === 'new-window') {
                createWindow();
                return;
              }

              win.webContents.send('menuClick', click);
            };
          }
          return submenuItem;
        });
      }
      return menuItem;
    });
    const menu = Menu.buildFromTemplate([ ...menuTemplate, ...template ]);
    Menu.setApplicationMenu(menu);
    event.returnValue = true;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady()
  .then(createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  app.quit();
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.

  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
