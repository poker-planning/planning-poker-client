# Poker Planning

## Client Side Server running on S3

https://s3.console.aws.amazon.com/s3/upload/planning-poker-electron?region=eu-west-1

## Server Side Server running on vanmarsbergen.com

https://vanmarsbergen.com:8888/eventbus

## Building

```
yarn build
```

and upload the assets in the `build` directory to S3
Update `"main"` in `package.json` to `"build/main.js"`

```
yarn dist
```

to create the Electron app which can be distributed

## Development

Update `"main"` in `package.json` to `"public/main.js"`

Run localhost:3000 with development environment

```
yarn start
```

Run:
```
yarn electron-dev
```

To open local Electron window which points to localhost:3000
