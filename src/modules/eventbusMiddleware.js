import EventBus from '@vertx/eventbus-bridge-client.js'

let eb = null

export default () => new Promise((resolve) => {
  eb = new EventBus('http://vanmarsbergen.com:8888/eventbus', {vertxbus_reconnect_attempts_max: 100})

  eb.enableReconnect(true)

  eb.onopen = () => {
    resolve(eb)
  }

  eb.onerror = () => {
    alert('Server is down. Try again later.')
  }
})
