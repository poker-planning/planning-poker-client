import { eventbus } from '../store'

export default (event, accessCode, team, onUpdate, listenFor = []) => {
  if (!listenFor.includes(event.type)) {
    return
  }

  switch (event.type) {
    case 'join':
      eventbus.publish(`team-${accessCode}`, {
        type: 'update',
        payload: team,
      })
      break
    case 'update':
      onUpdate(event.payload)
      break
    default:
      console.error(`Not implemented: ${JSON.stringify(event)}`)
  }
}
