export default (team, votes) =>
  !!(team && team.members && votes && Object.values(team.members)
    .filter(member => !member.observer).length === Object.values(votes).filter(item => item.length > 0).length)
