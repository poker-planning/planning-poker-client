export default arr => arr.length > 0 && arr.every(v => v === arr[0]);
