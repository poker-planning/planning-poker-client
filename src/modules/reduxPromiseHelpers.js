export function promisifyAction(actionCreator) {
  return (...args) => {
    const action = actionCreator(...args)
    let cancelled = false
    action.promise = new Promise((accept, reject) => {
      action.promiseMeta = {
        accept: (...acceptArgs) => !cancelled && accept(...acceptArgs),
        // eslint-disable-next-line prefer-promise-reject-errors
        reject: (...rejectArgs) => !cancelled && reject(...rejectArgs),
      }
    })
    action.promise.cancel = () => {
      cancelled = true
    }
    return action
  }
}

export const acceptAction = (action, ...args) => (
  action.promiseMeta
  && action.promiseMeta.accept
  && action.promiseMeta.accept(...args)
)
export const rejectAction = (action, ...args) => (
  action.promiseMeta
  && action.promiseMeta.reject
  && action.promiseMeta.reject(...args)
)
