const me = {
  SET_NAME: 'ME/SET_NAME',
  SET_TEAM: 'ME/SET_TEAM',
  SIGN_OUT: 'ME/SIGN_OUT',
}

export default me
