const votes = {
  VOTE: 'VOTES/VOTE',
  UPDATE_LAST_ROUND: 'VOTES/UPDATE_LAST_ROUND',
  NEW_ROUND: 'VOTES/NEW_ROUND',
}

export default votes
