const sessions = {
  CREATE: 'SESSIONS/CREATE',
  JOIN: 'SESSIONS/JOIN',
}

export default sessions
