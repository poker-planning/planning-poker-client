import teams from './teams'
import sessions from './sessions'
import me from './me'
import votes from './votes'
import eventbus from './eventbus'

const actionTypes = {
  TEAMS: teams,
  SESSIONS: sessions,
  ME: me,
  VOTES: votes,
  EVENTBUS: eventbus,
}

export default actionTypes
