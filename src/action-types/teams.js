const teams = {
  CREATE: 'TEAMS/CREATE',
  REMOVE_MEMBER: 'TEAMS/REMOVE_MEMBER',
}

export default teams
