import actionTypes from '../../action-types'

const newRound = (accessCode) => ({
  type: actionTypes.VOTES.NEW_ROUND,
  meta: {
    accessCode
  }
})

export default newRound
