import actionTypes from '../../action-types'

const goVote = (payload) => ({
  type: actionTypes.VOTES.VOTE,
  payload,
})

export default goVote
