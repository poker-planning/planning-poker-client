import actionTypes from '../../action-types'

const updateLastRound = (payload, accessCode) => ({
  type: actionTypes.VOTES.UPDATE_LAST_ROUND,
  payload,
  meta: {
    accessCode
  }
})

export default updateLastRound
