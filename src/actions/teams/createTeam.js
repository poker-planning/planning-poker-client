import actionTypes from '../../action-types'

const createTeam = (payload) => ({
  type: actionTypes.TEAMS.CREATE,
  payload,
})

export default createTeam
