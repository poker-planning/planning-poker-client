import actionTypes from '../../action-types'

const createTeam = (payload, accessCode) => ({
  type: actionTypes.TEAMS.REMOVE_MEMBER,
  payload,
  meta: { accessCode },
})

export default createTeam
