import actionTypes from '../../action-types'

const setMyName = (payload) => ({
  type: actionTypes.ME.SET_NAME,
  payload,
})

export default setMyName
