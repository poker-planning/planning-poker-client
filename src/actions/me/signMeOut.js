import actionTypes from '../../action-types'

const setMyTeam = () => ({
  type: actionTypes.ME.SIGN_OUT,
})

export default setMyTeam
