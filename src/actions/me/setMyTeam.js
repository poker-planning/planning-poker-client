import actionTypes from '../../action-types'

const setMyTeam = (payload) => ({
  type: actionTypes.ME.SET_TEAM,
  payload,
})

export default setMyTeam
