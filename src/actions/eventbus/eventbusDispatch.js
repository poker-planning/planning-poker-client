import actionTypes from '../../action-types'

const eventbusDispatch = (payload) => ({
  type: actionTypes.EVENTBUS.DISPATCH,
  payload,
})

export default eventbusDispatch
