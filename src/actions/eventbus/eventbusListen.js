import actionTypes from '../../action-types'

const eventbusListen = (payload) => ({
  type: actionTypes.EVENTBUS.LISTEN,
  payload,
})

export default eventbusListen
