import actionTypes from '../../action-types'

const joinSession = (accessCode, observer) => ({
  type: actionTypes.SESSIONS.JOIN,
  payload: {
    accessCode,
    observer
  },
})

export default joinSession
