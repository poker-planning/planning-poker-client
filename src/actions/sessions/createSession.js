import actionTypes from '../../action-types'

const createSession = (payload) => ({
  type: actionTypes.SESSIONS.CREATE,
  payload,
})

export default createSession
