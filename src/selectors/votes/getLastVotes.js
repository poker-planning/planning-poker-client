export default (state) =>
  (state.teams &&
    state.teams[state.me.team] &&
    state.teams[state.me.team].rounds &&
    state.teams[state.me.team].rounds[Object.values(state.teams[state.me.team].rounds).length - 1].votes
  ) || {}
