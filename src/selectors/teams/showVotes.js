import { createSelector } from 'reselect'
import getLastTeam from './getLastTeam'
import getLastVotes from '../votes/getLastVotes'
import showVotes from '../../modules/showVotes';

export default createSelector(
  getLastTeam,
  getLastVotes,
  (team, votes) => {
    return showVotes(team , votes)
  }
)
