import { connect } from 'react-redux'
import { promisifyAction } from '../../modules/reduxPromiseHelpers'
import createSession from '../../actions/sessions/createSession'
import getTeams from '../../selectors/teams/getTeams'
import setMyName from '../../actions/me/setMyName'
import SetupScreen from './SetupScreen'
import getMyName from '../../selectors/me/getMyName'

const stateToProps = (state) => ({
  teams: getTeams(state),
  myName: getMyName(state),
})

const dispatchToProps = {
  createSession: promisifyAction(createSession),
  setMyName,
}

export default connect(stateToProps, dispatchToProps)(SetupScreen)
