import React, { useState } from 'react';
import tw from 'twin.macro';

import PropTypes from 'prop-types';
import Layout from '../../components/Layout';
import Header from '../../components/Header';
import HeaderIntroduction from '../../components/HeaderIntroduction';
import Button from '../../components/Button';
import { Redirect } from 'react-router-dom';
import Share from '../../icons/Share';
import Card from '../../components/Card';
import InviteModal from '../../components/InviteModal';
import { css } from '@emotion/react';

const {
  clipboard,
} = window.require('electron');

const RoomScreen = ({
                      team,
                      votes,
                      showVotes,
                      goVote,
                      newRound,
                      isTeamOwner,
                      isObserver,
                      myVote,
                      myId,
                    }) => {
  const [ inviteTeam, setInviteTeam ] = useState(false);
  const [ copiedToClipboard, setCopiedToClipboard ] = useState(false);

  if (copiedToClipboard) {
    setTimeout(() => {
      setCopiedToClipboard(false);
    }, 3000);
  }

  if (!team || !team.cards) {
    return <Redirect to="/setup" />;
  }

  let average = null;
  let closestCard = null;
  let cardsCount = {};
  let membersPerVotes = {};
  let diverseCards = false;
  const questionRaised = [];

  if (showVotes) {
    let total = 0;

    for (const [ memberId, vote ] of Object.entries(votes)) {
      if (vote === '?') {
        questionRaised.push(team.members[memberId]);
      }
    }

    Object.values(votes)
      .forEach((vote, index) => {
        if (!isNaN(vote)) {
          total = total + +vote;
        }

        if (cardsCount[vote]) {
          cardsCount[vote] += 1;
        } else {
          cardsCount[vote] = 1;
        }

        if (!membersPerVotes[vote]) {
          membersPerVotes[vote] = [];
        }
        membersPerVotes[vote].push(team.members[Object.keys(votes)[index]]);
      });

    average = Math.ceil(total / Object.values(votes).filter(vote => !isNaN(vote)).length) || null;
    closestCard = team.cards.split(',')
      .map(value => +value)
      .reduce((prev, curr) => Math.abs(curr - average) <= Math.abs(prev - average) ? curr : prev);

    const scores = Object.keys(cardsCount).filter(vote => vote !== 'skip').sort((a, b) => a.localeCompare(b, 'en', { numeric: true }));
    diverseCards = team.cards.split(',').lastIndexOf(scores[scores.length - 1]) -
      team.cards.split(',').indexOf(scores[0]) > 1;
  }


  return (
    <Layout>
      <InviteModal
        show={inviteTeam}
        onClose={() => setInviteTeam(false)}
      >
        Team Code:
        <button
          css={tw`ml-1 focus:outline-none hover:text-gray-800`}
          onClick={() => {
            clipboard.writeText(team.accessCode);
            setCopiedToClipboard(true);
          }}
        >{team.accessCode}</button>
        <button
          css={tw`ml-2 focus:outline-none text-gray-400 hover:text-gray-600 text-xs`}
          onClick={() => {
            clipboard.writeText(team.accessCode);
            setCopiedToClipboard(true);
          }}
        >copy
        </button>
        <div
          css={[
            tw`text-xs ml-3 mt-1 italic text-green-300 dark:text-green-600 opacity-0 duration-500 transition-opacity`,
            copiedToClipboard && tw`opacity-100`,
          ]}
        >
          Team code copied to clipboard!
        </div>
      </InviteModal>
      <div
        css={tw`flex sticky top-0 pb-10 to-transparent bg-gradient-to-b from-white via-white dark:via-gray-900 dark:from-gray-900`}>
        <div css={tw`flex-1`}>
          <Header>Ready for estimation!</Header>
          <HeaderIntroduction>
            {Object.values(votes).length === 0 && 'Press Invite team so your team mates can join!'}
            {Object.values(votes).length > 0 && !showVotes && !votes[myId] && 'What are you waiting for? Don\'t leave your team members hanging!'}
            {Object.values(votes).length > 0 && !showVotes && votes[myId] && 'Waiting on the rest of your team to finish!'}
            {Object.values(votes).length > 0 && showVotes && isTeamOwner && 'Well done! When you ready, press New Round for a new round of estimations'}
            {Object.values(votes).length > 0 && showVotes && !isTeamOwner && 'Well done! When you ready, ask for a new round of estimations'}
          </HeaderIntroduction>
        </div>
        <div css={tw`flex-initial mt-10`}>
          <Button
            onClick={() => {
              setInviteTeam(true);
            }}
          >
            <span css={tw`font-mono text-sm`}>{team.accessCode}</span> <Share extraCss={tw`ml-2`} />
          </Button>
        </div>
      </div>

      <div css={tw`grid flex flex-wrap justify-evenly pb-8`}>
        {Object.values(team.members)
          .filter(member => !member.observer)
          .map(member => (
            <div key={member.id} css={tw`flex flex-col text-center mr-6 items-center`}>
              <Card
                small
                value={showVotes ? votes[member.id] : (votes[member.id] && votes[member.id] !== 'skip') ? '...' : votes[member.id]}
                highlight={member.id === myId && !!votes[member.id] || (Object.keys(votes).indexOf(member.id) > -1 && votes[member.id] === undefined)}
              />
              <div css={tw`-mt-4 font-bold`}>{member.name}</div>
            </div>
          ))}
      </div>

      {!isObserver && !showVotes && (
        <div css={tw`grid flex flex-nowrap justify-center space-x-6 pt-12 border-t dark:border-gray-700`}>
          {team.cards.split(',')
            .map(value => (
              <Card
                key={value}
                value={value}
                myVote={myVote}
                goVote={(vote) => {
                  if (!showVotes) {
                    if (myVote === vote.vote) {
                      goVote({ vote: '' });
                      return;
                    }

                    goVote(vote);
                  }
                }}
              />
            ))}
        </div>
      )}
      {showVotes && [
        ((!questionRaised || questionRaised.length === 0) && diverseCards) && (
          <div css={tw`text-center`}>
            <div css={tw`mb-6 p-3 bg-purple-50 dark:bg-gray-800 rounded-lg text-sm`}>
              Have a discussion, there is quite some <strong>difference in estimation</strong> in your team.
            </div>
          </div>
        ),
        questionRaised && questionRaised.length > 0 && (
          <div css={tw`text-center`}>
            <div css={tw`mb-6 p-3 bg-purple-50 dark:bg-gray-800 rounded-lg text-sm`}>
              Have a discussion,&nbsp;
              <strong>
                {questionRaised.reduce((acc, member, index, source) => {
                  const isLast = source.length > 1 && ((source.length - 1) === index);
                  acc += `${isLast ? ' and ' : (index !== 0 ? ', ' : '')}${member.name}`;
                  return acc;
                }, '')}
              </strong>
              &nbsp;
              {questionRaised.length > 1 ? 'have' : 'has'}
              &nbsp;some questions around this topic.
            </div>
          </div>
        ),
        <div
          css={css([
            tw`flex justify-center pt-10 border-t dark:border-gray-700 space-x-12`,
            ((questionRaised && questionRaised.length > 0) || diverseCards) && tw`opacity-10 hover:opacity-50 transition-opacity duration-700 ease-out`,
          ])}
        >
          {/** TODO: Highlight if a question mark is raised! **/}
          {(closestCard || average) && (
            <div css={tw`space-y-5`}>
              {closestCard && (
                <div css={tw`text-center`}>
                  <div css={tw`mb-2 opacity-80`}>Closest to card:</div>
                  <div css={tw`text-3xl`}>{closestCard}</div>
                </div>
              )}
              {average && (
                <div css={tw`text-center`}>
                  <div css={tw`mb-2 opacity-80`}>Average:</div>
                  <div css={tw`text-3xl`}>{average}</div>
                </div>
              )}
            </div>
          )}

          {Object.keys(cardsCount).length > 0 && (
            <div>
              <div css={tw`mb-2 opacity-80`}>Chosen cards:</div>
              <div css={tw`flex`}>
                {Object.keys(cardsCount)
                  .map(cardValue => {
                    return (
                      <div css={tw`flex flex-col text-center mr-6 items-center`}>
                        <Card small value={cardValue} />
                        <div css={tw`-mt-4`}>
                          {membersPerVotes[cardValue].map(member => (
                            <div css={tw`font-bold`}>{member.name}</div>
                          ))}
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          )}
        </div>,
      ]}
      <div css={tw`text-center mt-2`}>
        {!showVotes && !isObserver && (
          <Button
            onClick={() => goVote({ vote: 'skip' })}
            disabled={Object.values(team.members).length < 2}
            title={Object.values(team.members).length < 2 ? 'You can\'t skip when you are by yourself.' : 'Skip voting'}
          >
            Skip this round
          </Button>
        )}
        {isTeamOwner && (
          <Button onClick={() => newRound(team.accessCode)}>New Round</Button>
        )}
      </div>
    </Layout>
  );
};

RoomScreen.propTypes = {
  goVote: PropTypes.func.isRequired,
  newRound: PropTypes.func.isRequired,
  team: PropTypes.shape({
    cards: PropTypes.string,
  }),
  isTeamOwner: PropTypes.bool,
  myVote: PropTypes.string,
  myId: PropTypes.string,
};

RoomScreen.defaultProps = {
  team: null,
  isTeamOwner: false,
  myVote: null,
  myId: null,
};


export default RoomScreen;
