import { connect } from 'react-redux'
import RoomScreen from './RoomScreen'
import getLastTeam from '../../selectors/teams/getLastTeam'
import goVote from '../../actions/votes/goVote'
import getLastVotes from '../../selectors/votes/getLastVotes'
import isTeamOwner from '../../selectors/teams/isTeamOwner'
import newRound from '../../actions/votes/newRound'
import getMyVote from '../../selectors/votes/getMyVote'
import showVotes from '../../selectors/teams/showVotes'
import isObserver from '../../selectors/teams/isObserver'
import getMyId from '../../selectors/me/getMyId'

const stateToProps = (state) => ({
  team: getLastTeam(state),
  votes: getLastVotes(state),
  isTeamOwner: isTeamOwner(state),
  isObserver: isObserver(state),
  myVote: getMyVote(state),
  showVotes: showVotes(state),
  myId: getMyId(state),
})

const dispatchToProps = {
  goVote,
  newRound,
}

export default connect(stateToProps, dispatchToProps)(RoomScreen)
