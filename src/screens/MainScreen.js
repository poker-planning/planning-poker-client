import React from 'react'
import tw from 'twin.macro'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'
import Header from '../components/Header'

const MainScreen = () => (
  <Layout>
    <div>
      <Header>Layout</Header>
      <ul css={tw`list-disc ml-5`}>
        <li>
          <Link to="/setup" css={tw`border-b`}>Set up screen</Link>
          <ul css={tw`list-disc ml-5`}>
            <li>
              Create Session
              <ul css={tw`list-disc ml-5`}>
                <li>
                  Before Session Creation:
                  <ul css={tw`list-disc ml-5`}>
                    <li>Enter your name to create session</li>
                    <li>Choose card style: (fibbo, "scrum", tshirt, custom?)</li>
                    <li>Access Token en wachtwoord nodig?</li>
                    <li>Join als Product Owner</li>
                  </ul>
                </li>
                <li>
                  After Session Creation:
                  <ul css={tw`list-disc ml-5`}>
                    <li>Get access code</li>
                    <li>Share access code</li>
                    <li>Show TTL on access code</li>
                    <li>Revoke access code</li>
                    <li>Show that since you create the session you seems to be the Scrum Master</li>
                    <li>Enable auto join on the session for next app launch</li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <Link to="/join" css={tw`border-b`}>Join screen</Link>
              <ul css={tw`list-disc ml-5`}>
                <li>Fill in access code and optional password</li>
                <li>Enter your name</li>
                <li>Join als Product Owner</li>
                <li>Enable auto join on the session for next app launch</li>
              </ul>
            </li>
            <li>
              Show joined sessions
              <ul css={tw`list-disc ml-5`}>
                <li>Show if joined session as Scrum Master</li>
                <li>Per joined session, leave or destroy session (depending if you are the scrum master)</li>
                <li>Enable auto join on 1 of the joined sessions for next app launch</li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <Link to="/room" css={tw`border-b`}>Room screen</Link>
          <ul css={tw`list-disc ml-5`}>
            <li>Show cards</li>
            <li>Show joined team members</li>
          </ul>
        </li>

        <li>
          Uitdaging:
          <ul css={tw`list-disc ml-5`}>
            <li>Een app maken met distributed data store, dus data staat in de mac apps en alleen communicatie is via
              backend/server nodig (via eventbus)
            </li>
            <li>Joinen kan dan alleen als Scrum Master al connected is bij new session</li>
            <li>Create session maakt dan alleen lokaal data aan en optioneel een listener op `code+sm`, bij joinen maak
              je `code` aan.
            </li>
            <li>Als je joined vraag je eerst iets aan `code+sm` en als die niet reageert aan iemand anders met `code`
              voor de laatste data versie
            </li>
            <li>Elke update wordt naar alle `code`'s gestuurd en naar `code+sm` met alle data en een timestamp</li>
          </ul>
        </li>
      </ul>
    </div>
  </Layout>
)

MainScreen.propTypes = {}

MainScreen.defaultProps = {}

export default MainScreen
