import { connect } from 'react-redux'
import { promisifyAction } from '../../modules/reduxPromiseHelpers'
import joinSession from '../../actions/sessions/joinSession'
import setMyName from '../../actions/me/setMyName'
import JoinScreen from './JoinScreen'
import getMyName from '../../selectors/me/getMyName'
import getLastTeam from '../../selectors/teams/getLastTeam'

const stateToProps = (state) => ({
  team: getLastTeam(state),
  myName: getMyName(state),
})

const dispatchToProps = {
  joinSession: promisifyAction(joinSession),
  setMyName,
}

export default connect(stateToProps, dispatchToProps)(JoinScreen)
