import React, {
  useEffect,
  useState,
} from 'react'
import tw from 'twin.macro'
import Layout from '../../components/Layout'
import Header from '../../components/Header'
import HeaderIntroduction from '../../components/HeaderIntroduction'
import FormField from '../../components/FormField'
import Button from '../../components/Button'
import { useHistory } from 'react-router-dom'

const JoinScreen = ({
  joinSession,
  setMyName,
  myName,
  team,
}) => {
  const [name, setName] = useState(myName || '')
  const [code, setCode] = useState('')
  const [observer, setObserver] = useState(false)
  const [nameValid, setNameValid] = useState(true)
  const [loading, setLoading] = useState(false)

  const history = useHistory()

  useEffect(() => {
    if (loading && team) {
      history.push('room')
    }
  }, [team, loading])

  const validateName = () => name && name.length > 1 && name.replace(/\W/g, '').length > 1

  return (
    <Layout>
      <form
        css={tw`space-y-8 sm:space-y-5`}
        onSubmit={(event) => {
          event.preventDefault()

          setNameValid(validateName())

          if (
            !validateName()
          ) {
            return
          }

          setMyName(name)

          joinSession(code.toUpperCase(), !!observer)
            .promise
            .then(() => {
              setLoading(true)
            })
        }}
      >
        <div>
          <Header>Join a Session</Header>
          <HeaderIntroduction>Start a new Planning Poker session with your team</HeaderIntroduction>
        </div>
        <FormField
          label="What's your name?"
          name="name"
          value={name}
          onChange={(event) => setName(event.target.value)}
          valid={nameValid}
          errorMessage="Please fill in your (first) name, so team members know who is participating"
          inputProps={{
            autoFocus: true,
            value: name,
          }}
        />
        <FormField
          label="Fill in the Room code"
          name="code"
          onChange={(event) => setCode(event.target.value)}
        />
        <FormField
          label="Join without voting"
          name="observer"
          type="toggle"
          onChange={(enabled) => setObserver(enabled)}
          inputProps={{
            enabled: observer,
          }}
        />
        <div css={tw`text-center pt-5`}>
          <Button
            type="submit"
            large
            disabled={loading}
          >
            {loading && 'Signing you in...'}
            {!loading && 'Join your Team'}
          </Button>
        </div>
        {loading && (
          <div css={tw`text-center`}>
            <Button onClick={() => {
              setLoading(false)
            }}>Cancel and try again</Button>
          </div>
        )}
      </form>
    </Layout>
  )
}

export default JoinScreen
