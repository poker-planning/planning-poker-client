import React, { useEffect } from 'react';
import { Redirect } from 'react-router-dom';

const LeaveScreen = ({ removeMember, id, team, accessCode }) => {
  useEffect(() => {
    if (!team.members[id].owner) {
      removeMember(id, accessCode);
      return;
    }
    Object.values(team.members).forEach(member => {
      removeMember(member.id, accessCode);
    });
  }, [ removeMember, id, accessCode ]);

  return (<Redirect to="/" />);
};

export default LeaveScreen;
