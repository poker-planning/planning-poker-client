import { connect } from 'react-redux';
import LeaveScreen from './LeaveScreen';
import removeMember from '../../actions/teams/removeMember';
import getMyId from '../../selectors/me/getMyId';
import getMyTeamCode from '../../selectors/me/getMyTeamCode';
import signMeOut from '../../actions/me/signMeOut';
import getLastTeam from '../../selectors/teams/getLastTeam';

const stateToProps = (state) => ({
  id: getMyId(state),
  accessCode: getMyTeamCode(state),
  team: getLastTeam(state),
});

const dispatchToProps = {
  signMeOut,
  removeMember,
};

export default connect(stateToProps, dispatchToProps)(LeaveScreen);
