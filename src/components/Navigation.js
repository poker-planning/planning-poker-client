import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'
import NavigationItem from './NavigationItem'

const Navigation = ({
  title,
  items,
}) => (
  <ul
    style={{ WebkitAppRegion: 'no-drag' }}
    css={tw`mb-6`}
  >
    <li css={tw`text-xs font-bold text-gray-400 dark:text-gray-500 my-1`}>{title}</li>

    <ul css={tw`text-gray-600 dark:text-gray-400 space-y-1`}>
      {items && items.filter(Boolean).map(item => (
        <NavigationItem
          key={typeof item === 'object' ? item.key || item.label : item}
          {...(typeof item === 'object' ? item : { label: item })}
        />
      ))}
    </ul>
  </ul>

)

Navigation.propTypes = {
  title: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.shape(NavigationItem.propTypes),
      PropTypes.string,
    ]),
  ).isRequired,
}

export default Navigation
