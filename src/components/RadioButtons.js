import React from 'react'
import tw from 'twin.macro'
import PropTypes from 'prop-types'
import RadioButton from './RadioButton'

const RadioButtons = ({ name, value, onChange, options }) => (
  <div css={tw`max-w-lg`}>
    <div css={tw`space-y-4`}>
      {options && options.map(option => (
        <RadioButton
          {...option}
          key={option.id}
          checked={value === option.value}
          name={name}
          onChange={onChange}
        />
      ))}
    </div>
  </div>
)

RadioButtons.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
  })).isRequired,
}

export default RadioButtons
