import React from 'react'
import tw from 'twin.macro'
import { css } from '@emotion/react'

const TextInput = ({
  valid,
  ...props
}) => (
  <input
    css={css([
      tw`max-w-lg
      block
      w-full
      shadow-sm
      sm:max-w-xs
      sm:text-sm
      dark:bg-gray-800
      dark:text-gray-200
      p-1
      outline-none
      border
      border-gray-300
      dark:border-gray-800
      focus:outline-none
      focus:ring-2
      focus:ring-blue-300
      focus:dark:ring-blue-700
      rounded-md`,
      !valid && tw`border-red-500`,
    ])}
    {...props}
  />
)

export default TextInput
