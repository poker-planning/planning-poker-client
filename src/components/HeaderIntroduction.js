import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'

const HeaderIntroduction = ({children}) => (
  <p css={tw`mt-1 max-w-2xl text-sm text-gray-500 dark:text-gray-400`}>
    {children}
  </p>
)

HeaderIntroduction.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
}

export default HeaderIntroduction
