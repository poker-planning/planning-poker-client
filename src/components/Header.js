import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'

const Header = ({ children }) => (
  <h2
    css={tw`text-lg leading-6 font-medium text-gray-900 dark:text-gray-300 pt-10`}
    style={{ WebkitAppRegion: 'drag' }}
  >
    {children}
  </h2>
)

Header.propTypes = {
  children: PropTypes.string.isRequired,
}

export default Header
