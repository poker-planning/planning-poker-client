import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'

const Badge = ({ children }) => (
  <div
    css={tw`inline-flex
    items-center
    px-2.5
    py-0.5
    rounded-full
    text-xs
    font-medium
    bg-blue-200
    dark:bg-blue-600
    text-blue-900
    dark:text-white`}
  >
    {children}
  </div>
)

Badge.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
}

export default Badge
