import React from 'react'
import tw from 'twin.macro'
import PropTypes from 'prop-types'
import { css } from '@emotion/react'

const Button = ({
  large,
  ...props
}) => (
  <button
    type="button"
    css={css([
      tw`inline-flex
        items-center
        rounded-md
        text-gray-500
        dark:text-gray-300
        focus:outline-none
        py-2
        px-4
        disabled:opacity-50
    `,
      !large && tw`
        active:text-gray-800
        active:bg-gray-100
        dark:active:text-white
        dark:active:bg-gray-800
      `,
      large && tw`px-36
        py-3.5
        leading-5
        rounded-lg
        bg-gradient-to-br
        from-blue-500
        bg-blue-600
        text-white
        active:from-blue-500
        active:bg-blue-600
        dark:from-blue-700
        dark:bg-blue-600
        dark:active:from-blue-800
        dark:active:bg-blue-700
      `,
    ])}
    {...props}
  />
)

Button.propTypes = {
  large: PropTypes.bool,
}

Button.defaultProps = {
  large: false,
}


export default Button
