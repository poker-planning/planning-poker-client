import React from 'react';
import PropTypes from 'prop-types';
import tw from 'twin.macro';
import { css } from '@emotion/react';

const Card = ({
                value,
                myVote,
                highlight,
                goVote,
                small,
                extraCss,
              }) => (
  <button
    key={value}
    type="button"
    onClick={() => {
      if (goVote) {
        goVote({ vote: value });
      }
    }}
    css={css([
      tw`rounded-lg
      w-20
      py-10
      mb-6
      text-3xl
      border
      flex
      justify-center
      items-center
      focus:outline-none
      bg-gradient-to-br
      from-gray-50
      bg-gray-200
      border-gray-300
      shadow
      transition
      dark:from-gray-700
      dark:bg-gray-800
      dark:border-gray-900`,
      (myVote && myVote !== value) && tw`opacity-40 dark:opacity-20`,
      (myVote === value || highlight) && tw`
        from-blue-300
        bg-blue-500
        text-white
        dark:from-blue-600
        dark:bg-blue-800
        border-blue-900`,
      small && tw`
      w-14
      h-20
      py-6
      text-2xl`,
      small && !value && !highlight && tw`animate-pulse`,
      extraCss,
    ])}
  >
    {(value === 'skip') ? <span css={tw`text-sm transform rotate--45`}>Skipped</span> : value}
  </button>
);

Card.propTypes = {
  value: PropTypes.string.isRequired,
  goVote: PropTypes.func,
  myVote: PropTypes.string,
  highlight: PropTypes.bool,
  extraCss: PropTypes.object,
  small: PropTypes.bool,
};

Card.defaultProps = {
  myVote: null,
  goVote: null,
  extraCss: null,
  small: false,
  highlight: false,
};

export default Card;
