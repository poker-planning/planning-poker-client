import React from 'react'
import tw from 'twin.macro'
import { css } from '@emotion/react'

const Toggle = ({
  enabled,
  onChange,
}) => {
  return (
    <button
      type="button"
      aria-pressed={enabled}
      onClick={() => {
        onChange(!enabled)
      }}
      css={css([
        tw`bg-gray-200
        relative
        inline-flex
        flex-shrink-0
        h-6
        w-11
        border-2
        border-transparent
        rounded-full
        cursor-pointer
        transition-colors
        ease-in-out
        duration-200
        dark:focus:ring-blue-500
        dark:bg-gray-600
        focus:outline-none
        dark:focus:ring-offset-blue-500`,
        enabled && tw`bg-blue-500 dark:bg-blue-500`,
      ])}
    >
    <span
      css={tw`sr-only`}
    >
      Use setting
    </span>
      <span
        aria-hidden="true"
        css={css([
          tw`inline-block
          h-5
          w-5
          rounded-full
          bg-white
          dark:bg-gray-200
          shadow
          transform
          translate-x-0
          ring-0
          transition
          ease-in-out
          duration-200`,
          enabled && tw`translate-x-5`,
        ])}
      />
    </button>
  )
}

export default Toggle
