import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import tw from 'twin.macro';
import Button from './Button';
import Team from '../icons/Team';

const InviteModal = ({ show, onClose, children }) => {
  const [ render, setRender ] = useState(false);

  const onLocalClose = () => {
    setRender(false);
    setTimeout(() => {
      onClose();
    }, 225);
  };

  window.onkeyup = function(e) {
    if(e.code === 'Escape') {
      e.preventDefault();
      e.stopPropagation();
      onLocalClose()
    }
  }

  useEffect(() => {
    setRender(show)
  }, [show])

  if (!show) {
    return null;
  }

  return (
    <div css={tw`fixed z-10 inset-0 overflow-y-auto`}>
      <div css={tw`flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0`}>
        <div
          css={[
            tw`fixed inset-0 transition-opacity ease-out duration-200`,
            render && tw`opacity-100`,
            !render && tw`opacity-0`,
          ]}
          aria-hidden="true"
        >
          <div css={tw`absolute inset-0 bg-gray-500 opacity-75`} />
        </div>

        <span css={tw`hidden sm:inline-block sm:align-middle sm:h-screen`} aria-hidden="true">&#8203;</span>
        <div
          css={[
            tw`inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:p-6 ease-out duration-200`,
            render && tw`opacity-100`,
            !render && tw`opacity-0`,
          ]}
          role="dialog" aria-modal="true" aria-labelledby="modal-headline">
          <div>
            <div css={tw`mx-auto flex items-center justify-center h-12 w-12 rounded-full bg-green-100`}>
              <Team />

            </div>
            <div css={tw`mt-3 text-center sm:mt-5`}>
              <h3 css={tw`text-lg leading-6 font-medium text-gray-900`} id="modal-headline">
                Invite your team
              </h3>
              <div css={tw`mt-2`}>
                <p css={tw`text-sm text-gray-500`}>
                  {children}
                </p>
              </div>
            </div>
          </div>
          <div css={tw`mt-5 sm:mt-6`}>
            <Button large onClick={onLocalClose}>
              Close
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

InviteModal.propTypes = {
  show: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node,
};

InviteModal.defaultProps = {
  show: false,
  children: null,
};

export default InviteModal;
