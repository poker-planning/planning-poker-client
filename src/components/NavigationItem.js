import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'
import { Link } from 'react-router-dom'

const NavigationItem = ({
  prefix,
  label,
  to,
  action,
}) => (
  <li css={tw`flex justify-center`} className="group">
    <div css={tw`flex-initial`}>
      {prefix}
    </div>
    <div css={tw`flex-1`}>
      {to && (
        <Link to={to}>{label}</Link>
      )}
      {!to && label}
    </div>
    <div css={tw`flex-initial`}>
      {action}
    </div>
  </li>
)

NavigationItem.propTypes = {
  label: PropTypes.string.isRequired,
  to: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  action: PropTypes.node,
  prefix: PropTypes.node,
}

NavigationItem.defaultProps = {
  action: null,
  prefix: null,
  to: null,
}

export default NavigationItem
