import React from 'react';
import PropTypes from 'prop-types';
import tw from 'twin.macro';
import TextInput from './TextInput';
import RadioButtons from './RadioButtons';
import Toggle from './Toggle';

const FormField = ({ label, type, name, onChange, valid, addon, errorMessage, value, inputProps }) => (
  <div css={tw`mt-6 sm:mt-5 space-y-6 sm:space-y-5`}>
    <div
      css={tw`sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:dark:border-gray-800 sm:pt-5`}>
      <label htmlFor={name} css={tw`block text-sm font-medium text-gray-700 dark:text-gray-400 sm:mt-px sm:pt-2`}>
        {label}
      </label>
      <div css={tw`mt-1 sm:mt-1 sm:col-span-2`}>
        <div css={tw`flex`}>
          {(() => {
            switch (type) {
              case 'radio':
                return (
                  <RadioButtons
                    {...inputProps}
                    name={name}
                    value={value}
                    onChange={onChange}
                  />
                );
              case 'toggle':
                return (
                  <Toggle
                    {...inputProps}
                    name={name}
                    onChange={onChange}
                  />
                );
              default:
                return (
                  <TextInput
                    {...inputProps}
                    type={type}
                    name={name}
                    id={name}
                    onChange={onChange}
                    valid={valid}
                  />
                );
            }
          })()}
          {addon && (
            <div css={tw`ml-4 inline-block`}>
              {addon}
            </div>
          )}
        </div>
        {(!valid && errorMessage) && (
          <p css={tw`mt-2 text-sm text-red-600`}>{errorMessage}</p>
        )}
      </div>
    </div>
  </div>
);

FormField.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  inputProps: PropTypes.shape(),
  valid: PropTypes.bool,
  errorMessage: PropTypes.string,
  addon: PropTypes.node,
};

FormField.defaultProps = {
  type: 'text',
  inputProps: {},
  valid: true,
  addon: null,
  errorMessage: 'This field is either empty or incorrect. I know you can do better than this!',
};

export default FormField;
