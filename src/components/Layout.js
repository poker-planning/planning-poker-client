import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'
import Sidebar from './Sidebar'

const Layout = ({ children }) => (
  <div css={tw`flex dark:bg-gray-900 dark:text-gray-400 overflow-hidden h-screen select-none`}>
    <Sidebar />
    <div css={tw`flex-1 px-8 overflow-auto ml-48 pb-10 bg-white dark:bg-gray-900 relative`}>
      {children}
    </div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
