import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'

const RadioButton = ({label, id, name, value, onChange, ...props}) => (
  <div className="flex items-center content-center">
    <input
      id={id}
      name={name}
      value={value}
      onChange={onChange}
      type="radio"
      css={tw`inline-block
      outline-none
      active:ring-blue-300
      focus:ring-blue-500
      dark:bg-blue-300
      h-4
      w-4
      text-blue-600
      border-gray-300`}
      {...props}
    />
    <label
      htmlFor={id}
      css={tw`ml-1.5
      inline-block
      text-sm
      font-medium
      text-gray-700
      dark:text-gray-400`}
    >
      {label}
    </label>
  </div>
)

RadioButton.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
}

export default RadioButton
