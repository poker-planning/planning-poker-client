import React, { useEffect } from 'react';
import tw from 'twin.macro';
import {
  Link,
  useHistory,
} from 'react-router-dom';
import party from 'party-js';
import Navigation from '../Navigation';
import Ban from '../../icons/Ban';
import Badge from '../Badge';
import Remove from '../../icons/Remove';
import showVotes from '../../modules/showVotes';
import allEqual from '../../modules/allEqual';

const {
  ipcRenderer,
} = window.require('electron');
const { platform } = window.require('process');
const isMac = platform === 'darwin';

const Sidebar = ({
                   team,
                   votes,
                   isTeamOwner,
                   myId,
                   newRound,
                   removeMember,
                 }) => {
  const history = useHistory();
  useEffect(() => {
    window.onbeforeunload = function () {
      history.push('leave');
    };

    ipcRenderer.on('menuClick', (event, { type }) => {
      switch (type) {
        case 'new-session':
          history.push('setup');
          break;
        case 'new-round':
          if (team && team.accessCode) {
            newRound(team.accessCode);
          }
          break;
        case 'join-session':
          history.push('join');
          break;
        case 'sign-out':
          history.push('leave');
          break;
        case 'learn-more':
          history.push('help');
          break;
        default:
          console.error(`Type ${type} is not defined as menuClick`);
          break;
      }
    });

    const menu = [
      {
        label: 'File',
        submenu: [
          // {
          //   label: 'New Window',
          //   click: { type: 'new-window' },
          //   accelerator: 'CommandOrControl+Shift+N',
          // },
          {
            label: 'New Session',
            click: { type: 'new-session' },
            accelerator: 'CommandOrControl+N',
          },
          {
            label: 'Join Session',
            click: { type: 'join-session' },
            accelerator: 'CommandOrControl+O',
          },
          { type: 'separator' },
          isMac ? { role: 'close' } : { role: 'quit' },
        ],
      },
      {
        role: 'editMenu',
      },
      {
        label: 'View',
        submenu: [
          { role: 'reload' }, // Only show for devs
          { role: 'forceReload' }, // Only show for devs
          { role: 'toggleDevTools' }, // Only show for devs
          { type: 'separator' },
          { role: 'togglefullscreen' },
        ],
      },
      {
        role: 'windowMenu',
      },
      {
        role: 'help',
        submenu: [
          {
            label: 'Learn More',
            click: { type: 'learn-more' },
          },
        ],
      },
    ];

    if (team && team.accessCode) {
      menu[0].submenu.splice(0, 2,
        // {
        //   label: 'New Window',
        //   click: { type: 'new-window' },
        //   accelerator: 'CommandOrControl+Shift+N',
        // },
        {
          label: 'New Round',
          click: { type: 'new-round' },
          accelerator: 'CommandOrControl+N',
        },
        {
          type: 'separator',
        },
        {
          label: 'Sign out',
          click: { type: 'sign-out' },
        },
      );
    }
    ipcRenderer.sendSync('updateMenu', menu);
  }, [ history, newRound, team ]);

  useEffect(() => {
    let timeout1, timeout2;
    const members = team && Object.values(team.members)
      .filter(member => !member.observer);

    if (team &&
      members.length > 1 &&
      Object.values(votes).filter(v => v !== '?' && v !== '' && v !== 'skip').length > 1 &&
      members.length === Object.values(votes).filter(v => v !== '?' && v !== '').length &&
      allEqual(Object.values(votes).filter(v => v !== 'skip' && v !== '?' && v !== ''))
    ) {
      party.position(100, 500, {
        count: 60,
        countVariation: 0.5,
        angleSpan: 180,
        shape: 'circle',
        angle: -20,
        yVelocity: -300,
        yVelocityVariation: 1,
        rotationVelocityLimit: 6,
        scaleVariation: 0.8,
      });

      timeout1 = setTimeout(() => {
        party.position(800, 400, {
          count: 60,
          countVariation: 0.5,
          angleSpan: 180,
          angle: 30,
          shape: 'circle',
          yVelocity: -300,
          yVelocityVariation: 1,
          rotationVelocityLimit: 6,
          scaleVariation: 0.8,
        });
      }, 200);

      timeout2 = setTimeout(() => {
        party.position(400, 200, {
          count: 100,
          countVariation: 2.5,
          angleSpan: 180,
          shape: 'circle',
          angle: 5,
          yVelocity: -500,
          yVelocityVariation: 6,
          rotationVelocityLimit: 6,
          scaleVariation: 1.8,
        });

        return () => {
          clearTimeout(timeout1);
          clearTimeout(timeout2);
        };
      }, 75);
    }
  }, [ votes, team ]);

  return (
    <div
      css={tw`flex-initial
        w-48
        min-w-min
        bg-gray-200
        dark:bg-gray-800
        bg-opacity-80
        min-h-screen
        pt-11
        px-4
        fixed
        text-sm
        select-none`}
      style={{ WebkitAppRegion: 'drag' }}
    >
      {team && team.members && (
        <Link to="/room" css={tw`cursor-auto`}>
          <Navigation
            title="Your Team"
            items={Object.values(team.members || {})
              .map(member => ({
                key: member.id,
                label: member.name,
                prefix: isTeamOwner && member.id !== myId && (
                  <button onClick={() => {
                    removeMember(member.id, team.accessCode);
                  }} css={tw`hidden group-hover:inline-block mr-1.5 text-red-700 dark:text-red-400`}>
                    <Remove />
                  </button>
                ),
                action: member.observer ?
                  (
                    <span
                      css={tw`mx-1.5 inline-block`}
                      title="This user is an observer, so they can't goVote"
                    >
                      <Ban extraCss={tw`w-4 h-4`} />
                    </span>
                  ) :
                  votes && votes[member.id] &&
                  <Badge>{showVotes(team, votes) || member.id === myId ? votes[member.id] : '...'}</Badge>,
              }))}
          />
        </Link>
      )}
      <Navigation
        title="Options"
        items={[
          !team ? {
            label: 'Start new Session',
            to: '/setup',
          } : null,
          !team ? {
            label: 'Join your team',
            to: '/join',
          } : null,
          team ? {
            label: 'Leave Session',
            to: '/leave',
          } : null,
        ]}
      />
    </div>
  );
};

export default Sidebar;
