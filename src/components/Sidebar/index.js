import { connect } from 'react-redux'
import Sidebar from './Sidebar.js'
import getLastTeam from '../../selectors/teams/getLastTeam'
import getLastVotes from '../../selectors/votes/getLastVotes'
import removeMember from '../../actions/teams/removeMember'
import isTeamOwner from '../../selectors/teams/isTeamOwner'
import getMyId from '../../selectors/me/getMyId'
import newRound from '../../actions/votes/newRound'

const stateToProps = (state) => ({
  team: getLastTeam(state),
  votes: getLastVotes(state),
  isTeamOwner: isTeamOwner(state),
  myId: getMyId(state),
})

const dispatchToProps = {
  removeMember,
  newRound,
}

export default connect(stateToProps, dispatchToProps)(Sidebar)
