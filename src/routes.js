import React from 'react'
import {
  HashRouter,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom'
import MainScreen from './screens/MainScreen'
import SetupScreen from './screens/SetupScreen'
import RoomScreen from './screens/RoomScreen'
import JoinScreen from './screens/JoinScreen'
import LeaveScreen from './screens/LeaveScreen'

export default function routes() {
  return (
    <HashRouter>
      <Switch>
        <Route exact path="/help" component={MainScreen} />
        <Route exact path="/setup" component={SetupScreen} />
        <Route exact path="/room" component={RoomScreen} />
        <Route exact path="/join" component={JoinScreen} />
        <Route exact path="/leave" component={LeaveScreen} />
        <Redirect to="/setup" />
      </Switch>
    </HashRouter>
  )
}

