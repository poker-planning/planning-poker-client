import React from 'react'
import PropTypes from 'prop-types'
import tw from 'twin.macro'
import { css } from '@emotion/react'

const Ban = ({extraCss}) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
    css={css([
      tw`w-5 h-5`,
      extraCss,
    ])}
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636"
    />
  </svg>
)

Ban.propTypes = {
  extraCss: PropTypes.object
}

Ban.defaultProps = {
  extraCss: null
}

export default Ban
