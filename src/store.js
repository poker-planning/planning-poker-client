import createSagaMiddleware from 'redux-saga'
import {
  applyMiddleware,
  combineReducers,
  createStore,
} from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import reducers from './reducers'
import sagas from './sagas'

const sagaMiddleware = createSagaMiddleware()

const store = createStore(combineReducers({
  ...reducers,
}), {}, composeWithDevTools(applyMiddleware(sagaMiddleware)))

sagaMiddleware.run(sagas)

export {
  store,
}
