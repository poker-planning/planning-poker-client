import actionTypes from '../action-types'
import {
  put,
  select,
  takeLatest,
} from '@redux-saga/core/effects'
import getMyId from '../selectors/me/getMyId'
import getMyName from '../selectors/me/getMyName'
import { acceptAction } from '../modules/reduxPromiseHelpers'
import eventbusListen from '../actions/eventbus/eventbusListen'
import eventbusDispatch from '../actions/eventbus/eventbusDispatch'
import setMyTeam from '../actions/me/setMyTeam'

const onJoinSession = function* onJoinSession() {
  yield takeLatest(actionTypes.SESSIONS.JOIN, function* onJoinSession(action) {
    const {
      payload: { accessCode, observer },
    } = action

    const myId = yield select(getMyId)
    const myName = yield select(getMyName)

    yield put(setMyTeam(accessCode))

    yield put(eventbusListen({
      address: `team-${accessCode}`,
      dispatchType: actionTypes.EVENTBUS.MEMBER,
    }))

    yield put(eventbusListen({
      address: `team-${accessCode}-${myId}`,
      dispatchType: actionTypes.EVENTBUS.ME,
    }))

    yield put(eventbusDispatch({
      address: `team-${accessCode}-owner`,
      payload: {
        type: 'join',
        payload: {
          user: {
            id: myId,
            name: myName,
            observer: !!observer,
          },
        },
      },
    }))

    acceptAction(action)
  })
}

export default onJoinSession
