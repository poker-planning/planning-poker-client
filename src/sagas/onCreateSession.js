// Create team and team code
// payload: name, observer, customCards, cardStyle

// Add members to team where current user is set with name and observer boolean

// Start eventbus listener on team-CODE
// Start eventbus listener on team-CODE-owner

import actionTypes from '../action-types'
import {
  put,
  select,
  takeLatest,
} from '@redux-saga/core/effects'
import createTeam from '../actions/teams/createTeam'
import { acceptAction } from '../modules/reduxPromiseHelpers'
import getMyId from '../selectors/me/getMyId'
import setMyTeam from '../actions/me/setMyTeam'
import eventbusListen from '../actions/eventbus/eventbusListen'
import newRound from '../actions/votes/newRound'
import eventbusDispatch from '../actions/eventbus/eventbusDispatch';

const onCreateSession = function* onCreateSession() {
  yield takeLatest(actionTypes.SESSIONS.CREATE, function* onCreateSession(action) {
    const {
      payload: {
        name,
        observer,
        customCards,
        cardStyle,
        accessCode,
      },
    } = action

    const id = yield select(getMyId)

    yield put(eventbusListen({
      address: `team-${accessCode}-${id}`,
      dispatchType: actionTypes.EVENTBUS.ME,
    }))

    yield put(eventbusDispatch({
      address: `team-${accessCode}`,
      payload: {
        type: 'available',
        payload: {
          accessCode,
          memberId: id
        },
      },
    }))

    yield put(createTeam({
      members: {
        [id]: {
          owner: true,
          id,
          name,
          observer,
        },
      },
      cards: cardStyle === 'custom' ? customCards : cardStyle,
      accessCode,
    }))
    yield put(newRound(accessCode))

    yield put(setMyTeam(accessCode))

    yield put(eventbusListen({
      address: `team-${accessCode}-owner`,
      dispatchType: actionTypes.EVENTBUS.OWNER,
    }))

    yield put(eventbusListen({
      address: `team-${accessCode}`,
      dispatchType: actionTypes.EVENTBUS.MEMBER,
    }))

    acceptAction(action, true)
  })
}

export default onCreateSession
