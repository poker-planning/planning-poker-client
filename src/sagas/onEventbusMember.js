import {
  put,
  takeLatest,
} from '@redux-saga/core/effects';
import actionTypes from '../action-types';
import createTeam from '../actions/teams/createTeam';
import goVote from '../actions/votes/goVote';
import signMeOut from '../actions/me/signMeOut';
import eventbusDispatch from '../actions/eventbus/eventbusDispatch';

const onEventbusMember = function* onEventbusMember() {
  yield takeLatest([ actionTypes.EVENTBUS.MEMBER, actionTypes.EVENTBUS.ME ], function* onEventbusMember(action) {
    const { payload } = action;

    if (payload.type === 'update' && payload.payload) {
      yield put(createTeam(payload.payload));
    }

    if (payload.type === 'remove') {
      // alert('You\'ve been removed from your team.');
      yield put(signMeOut());
      // This kicks the removed member of in a broken state. Only fix for now is a reload
    }

    if (payload.type === 'available' && payload.payload !== 'taken') {
      yield put(eventbusDispatch({
        address: `team-${payload.payload.accessCode}-${payload.payload.memberId}`,
        payload: {
          type: 'available',
          payload: 'taken',
        },
      }));
    }

    if (payload.type === 'available' && payload.payload === 'taken') {
      alert('This room is already taken.');
      yield put(signMeOut());
    }

    if (payload.type === 'vote') {
      const { vote, userId } = payload.payload;
      yield put(goVote({ vote, userId }));
    }
  });
};

export default onEventbusMember;
