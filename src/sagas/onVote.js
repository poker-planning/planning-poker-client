import actionTypes from '../action-types';
import {
  put,
  select,
  takeLatest,
} from '@redux-saga/core/effects';
import { acceptAction } from '../modules/reduxPromiseHelpers';
import getMyId from '../selectors/me/getMyId';
import updateLastRound from '../actions/votes/updateLastRound';
import getLastVotes from '../selectors/votes/getLastVotes';
import eventbusDispatch from '../actions/eventbus/eventbusDispatch';
import getMyTeamCode from '../selectors/me/getMyTeamCode';

const onVote = function* onVote() {
  yield takeLatest(actionTypes.VOTES.VOTE, function* onVote(action) {
    const {
      payload: {
        vote,
        userId,
      },
    } = action;

    const id = yield select(getMyId);
    const lastRound = yield select(getLastVotes);
    const accessCode = yield select(getMyTeamCode);

    lastRound[userId || id] = vote;

    yield put(updateLastRound({
      [userId || id]: vote,
    }, accessCode));

    if (!userId) { // TODO: Fix that you get the entire "round" object and not just a vote, since it gets you out of sync
      yield put(eventbusDispatch({
        address: `team-${accessCode}`,
        payload: {
          type: 'vote',
          payload: {
            userId: id,
            vote: vote,
          },
        },
      }));
    }

    acceptAction(action, true);
  });
};

export default onVote;
