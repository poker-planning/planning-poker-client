import {
  all,
  spawn,
} from 'redux-saga/effects'
import onCreateSession from './onCreateSession'
import onVote from './onVote'
import onJoinSession from './onJoinSession'
import onEventbus from './onEventbus'
import onEventbusOwner from './onEventbusOwner'
import onEventbusMember from './onEventbusMember'
import onNewRound from './onNewRound'
import onRemoveMember from './onRemoveMember'

const rootSaga = function* rootSaga() {
  yield all([
    spawn(onCreateSession),
    spawn(onJoinSession),
    spawn(onVote),
    spawn(onEventbus),
    spawn(onEventbusOwner),
    spawn(onEventbusMember),
    spawn(onNewRound),
    spawn(onRemoveMember),
  ])
}

export default rootSaga
