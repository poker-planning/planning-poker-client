import actionTypes from '../action-types'
import {
  put,
  select,
  takeLatest,
} from '@redux-saga/core/effects'
import eventbusDispatch from '../actions/eventbus/eventbusDispatch'
import getLastTeam from '../selectors/teams/getLastTeam'

const onNewRound = function* onNewRound() {
  yield takeLatest(actionTypes.VOTES.NEW_ROUND, function* onNewRound(action) {
    const {
      meta: {
        accessCode,
      },
    } = action

    const team = yield select(getLastTeam)

    yield put(eventbusDispatch({
      address: `team-${accessCode}`,
      payload: {
        type: 'update',
        payload: team,
      },
    }))
  })
}

export default onNewRound
