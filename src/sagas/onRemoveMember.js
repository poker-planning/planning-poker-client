import actionTypes from '../action-types'
import {
  put,
  takeLatest,
} from '@redux-saga/core/effects'
import eventbusDispatch from '../actions/eventbus/eventbusDispatch'
import newRound from '../actions/votes/newRound'

const onRemoveMember = function* onRemoveMember() {
  yield takeLatest(actionTypes.TEAMS.REMOVE_MEMBER, function* onRemoveMember(action) {
    const {
      payload,
      meta: {
        accessCode,
      },
    } = action

    yield put(eventbusDispatch({
      address: `team-${accessCode}-${payload}`,
      payload: {
        type: 'remove',
      },
    }))

    yield put(newRound(accessCode))
  })
}

export default onRemoveMember
