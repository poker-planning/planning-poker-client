// Create team and team code
// payload: name, observer, customCards, cardStyle

// Add members to team where current user is set with name and observer boolean

// Start eventbus listener on team-CODE
// Start eventbus listener on team-CODE-owner

import {
  call,
  put,
  race,
  take,
} from '@redux-saga/core/effects';
import eventbusMiddleware from '../modules/eventbusMiddleware';
import { eventChannel } from '@redux-saga/core';
import actionTypes from '../action-types';

const createEbChannel = (eventbus, address, dispatchType) => {
  return eventChannel(emit => {
    eventbus.registerHandler(address, (error, message) => {
      if (message) {
        emit({
          type: dispatchType,
          payload: message.body,
        });
      }
    });

    return () => eventbus.unregisterHandler(address);
  });
};


let socketChannels = [];
let eventbus;

const onEventbus = function* onEventbus() {
  eventbus = yield call(eventbusMiddleware);

  while (true) {
    let racers = {
      listen: take(actionTypes.EVENTBUS.LISTEN),
      dispatch: take(actionTypes.EVENTBUS.DISPATCH),
      unregister: take(actionTypes.ME.SIGN_OUT),
    };

    if (socketChannels.length > 0) {
      socketChannels.forEach((value, index) => {
        racers[`socketChannel${index}`] = take(value);
      });
    }

    const {
      listen,
      dispatch,
      unregister,
      ...allSocketHandlers
    } = yield race(racers);

    if (listen) {
      const sc = yield call(createEbChannel, eventbus, listen.payload.address, listen.payload.dispatchType);
      socketChannels.push(sc);
    }

    if (dispatch) {
      eventbus.publish(dispatch.payload.address, dispatch.payload.payload);
    }

    if (unregister) {
      socketChannels.forEach(socketChannels => {
        socketChannels.close();
      });
      socketChannels = []
    }

    if (Object.values(allSocketHandlers)) {
      for (let socketHandler in allSocketHandlers) {
        yield put(allSocketHandlers[socketHandler]);
      }
    }
  }
};

export default onEventbus;
