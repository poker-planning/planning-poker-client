// Create team and team code
// payload: name, observer, customCards, cardStyle

// Add members to team where current user is set with name and observer boolean

// Start eventbus listener on team-CODE
// Start eventbus listener on team-CODE-owner

import {
  put,
  select,
  takeLatest,
} from '@redux-saga/core/effects';
import actionTypes from '../action-types';
import getLastTeam from '../selectors/teams/getLastTeam';
import eventbusDispatch from '../actions/eventbus/eventbusDispatch';
import createTeam from '../actions/teams/createTeam';
import signMeOut from '../actions/me/signMeOut';

const onEventbusOwner = function* onEventbusOwner() {
  yield takeLatest(actionTypes.EVENTBUS.OWNER, function* onEventbusOwner(action) {
    const {
      payload: {
        type,
        payload,
      },
    } = action;

    const { accessCode } = yield select(getLastTeam);

    if (type === 'join') {
      const existingTeam = yield select(getLastTeam);

      yield put(createTeam({
        ...existingTeam,
        members: {
          ...existingTeam.members,
          [payload.user.id]: {
            ...payload.user,
            owner: false,
          },
        },
      }));

      const team = yield select(getLastTeam);

      yield put(eventbusDispatch({
        address: `team-${accessCode}`,
        payload: {
          type: 'update',
          payload: team,
        },
      }));
    }
  });
};

export default onEventbusOwner;
