import actionTypes from '../action-types';

const initialState = {};

const teams = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ME.SIGN_OUT:
      return {
        ...initialState,
      };
    case actionTypes.TEAMS.CREATE:
      return {
        ...state,
        [action.payload.accessCode]: {
          rounds: [],
          members: {},
          ...action.payload,
        },
      };
    case actionTypes.VOTES.UPDATE_LAST_ROUND:
      const rounds = [ ...state[action.meta.accessCode].rounds ].sort(item => item.order);
      const lastRound = rounds[rounds.length - 1];
      lastRound.votes = {
        ...lastRound.votes,
        ...action.payload,
      };

      rounds.splice(rounds.length - 1, 1, lastRound);

      return {
        ...state,
        [action.meta.accessCode]: {
          ...state[action.meta.accessCode],
          rounds,
        },
      };
    case actionTypes.VOTES.NEW_ROUND:
      return {
        ...state,
        [action.meta.accessCode]: {
          ...state[action.meta.accessCode],
          rounds: [
            ...state[action.meta.accessCode].rounds,
            {
              order: new Date().getTime(),
              votes: {},
            },
          ],
        },
      };
    case actionTypes.TEAMS.REMOVE_MEMBER:
      const members = { ...state[action.meta.accessCode].members };
      delete members[action.payload];

      return {
        ...state,
        [action.meta.accessCode]: {
          ...state[action.meta.accessCode],
          members,
        },
      };
    default:
      return state;
  }
};

export default teams;
