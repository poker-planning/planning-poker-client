import teams from './teams'
import me from './me'

const reducers = {
  teams,
  me,
}

export default reducers
