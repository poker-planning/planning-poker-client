import actionTypes from '../action-types'
import uuid from '../modules/uuid'

const initialState = {
  id: uuid(),
  name: '',
  team: null,
}

const teams = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ME.SET_NAME:
      return {
        ...state,
        name: action.payload,
      }
    case actionTypes.ME.SET_TEAM:
      return {
        ...state,
        team: action.payload,
      }
    case actionTypes.ME.SIGN_OUT:
      return {
        ...state,
        team: null,
      }
    default:
      return state
  }
}

export default teams
